import math,random

polys=[]
archive={}

def gen_polys(num, min_param, max_param, length):
    for i in range(num):
        new_poly=[]
        for x in range(length):
            new_poly.append(random.uniform(min_param,max_param))
        polys.append(new_poly)

def eval_poly(poly, in_vals, desired_vals): #The function, interval minimum & maximum, and number of subdivisions
    error=0
    for z in range(len(in_vals)):
        l=len(poly)
        poly_val=0
        for x in range(l): #For every num in gene
            poly_val+=poly[x]*in_vals[z]**((l-1)-x)#Add output to out_vals
        error += abs(poly_val-desired_vals[z])
    return error

def hill_climb(max_deltas, in_vals, desired_vals): #This is inefficient, and may be slowing the program down
    for poly in polys[:]:
        best_poly=poly
        best_score=eval_poly(poly, in_vals, desired_vals)
        for max_delta in max_deltas:
            for x in range(len(poly)):
                new_poly=poly[:]
                new_poly[x]+=max_delta
                score=eval_poly(new_poly, in_vals, desired_vals)
                if score < best_score:
                    best_poly=new_poly
                    best_score=score
                
                new_poly=poly[:]
                new_poly[x]-=max_delta
                score=eval_poly(new_poly, in_vals, desired_vals)
                if score < best_score:
                    best_poly=new_poly
                    best_score=score
                    
        polys.remove(poly)
        if best_poly==poly: #If it finds a max, archive it
            archive[best_score]=poly
        else: #If not, add the next poly
            polys.append(best_poly)

def gen_poly_str(poly):
    out=""
    l=len(poly)
    for x in range(l):
        out += str(poly[x])+"x^"+str(((l-1)-x))
        if x != l-1:
            out += "+"
    return out

def main():
    in_vals=[]
    desired_vals=[]
    for x in xrange(301):
        in_vals.append(x*(math.pi/2.0)/300.0)
    for n in in_vals:
        desired_vals.append(math.sin(n))
        
    gen_polys(100,-1,1,8)
    
    round_num=0
    while len(polys) > 0:
        hill_climb([.1,.01,.001],in_vals,desired_vals)
        print round_num
        if round_num % 100 == 0:
            best_score=100000
            for p in polys:
                score=eval_poly(p,in_vals,desired_vals)
                if score < best_score:
                    best_score=score
            print "BEST SCORE OF *REMAINING* POLYS: %d" % best_score
        round_num+=1
    l=archive.keys()
    l.sort()
    print gen_poly_str(archive[l[0]])
    print "Score: %d" % l[0]
main()
raw_input("Done")
